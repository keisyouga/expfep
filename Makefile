# create starpack expfep-linux-x86_64
# required:
#   sdx
#     command
#   tclkit
#     command
#   tclkit-runtime
#     copy /path/to/tclkit to ./tclkit-runtime if not exist.
#     you can reduce binary size by preparing small tclkit-runtime.

SDX != command -v sdx
TCLKIT != command -v tclkit
TCLKIT_RUNTIME = tclkit-runtime
VFSDIR = expfep.vfs

# create linux x86-64 starpack
expfep-linux-x86_64: expfep.vfs/main.tcl ${TCLKIT_RUNTIME}
	sdx wrap expfep -runtime ${TCLKIT_RUNTIME}
	mv expfep expfep-linux-x86_64

# copy tclkit to current directory as ${TCLKIT_RUNTIME}
${TCLKIT_RUNTIME}:
	cp ${TCLKIT} ${TCLKIT_RUNTIME}
